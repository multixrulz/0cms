#!/usr/bin/python

# This file is part of 0cms.
#
# 0cms is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys
import os
import fnmatch
import subprocess
from collections import namedtuple
from types import MethodType

# Define my own relpath that removes the useless /./ entries for the top level.
def relative_path(path, start):
    reldir = os.path.relpath(path, start)
    if reldir == '.':
        return '' # Keep redundant . out of paths
    else:
        return reldir

class PageComponent():
    def __init__(self, content):
        self._content = content

    def __str__(self):
        if callable(self._content):
            self._content = self._content()

class Markdown(PageComponent):
    _default_extensions = []

    @classmethod
    def default_extensions(cls, extensions):
        cls._default_extensions = extensions

    def __init__(self, *args, extensions=[], overwrite=False):
        super().__init__(*args)
        self._overwrite = overwrite
        self._extensions = extensions

    def __str__(self):
        super().__str__()
        import markdown
        if self._overwrite:
            extensions = self._extensions
        else:
            extensions = self._default_extensions.copy()
            extensions.extend(self._extensions)
        return markdown.markdown(
            self._content,
            extensions=extensions
            )

class Asciidoc(PageComponent):
    def __str__(self):
        super().__str__()
        return subprocess.run(
            ['asciidoc',
                '-b', 'html5', # Generate html5
                '-s',
                 '-'], # Take input from stdin
            input=self._content,
            text=True,
            capture_output=True
            ).stdout

class HtmlDiv(PageComponent):
    def __init__(self, *content, htmlid="", htmlclass=""):
        super().__init__(content)
        self._id = ' id="{}"'.format(htmlid) if htmlid != '' else ''
        self._class = ' class="{}"'.format(htmlclass) if htmlclass != '' else ''

    def __str__(self):
        super().__str__()
        try:
            content_strings = (str(c) for c in self._content)
        except TypeError:
            content_strings = str(c)
        return ("<div{}{}>\n".format(self._id, self._class) +
            "{}".format('\n'.join(content_strings)) +
            "\n</div>")

class HtmlImage(PageComponent):
    def __init__(self, src, alt="", htmlid="", htmlclass=""):
        self._content = src # Could be a callable to match the other classes
        self._alt = ' alt="{}"'.format(alt) if alt != '' else ''
        self._id = ' id="{}"'.format(htmlid) if htmlid != '' else ''
        self._class = ' class="{}"'.format(htmlclass) if htmlclass != '' else ''

    def __str__(self):
        super().__str__()
        return ('<img src="{}"{}{}{}>\n'.format(self._content,
            self._alt, self._id, self._class))

class Html(PageComponent):
    def __str__(self):
        super().__str__()
        return self._content

### Utility code
# Handle the command line
def executable():
    return sys.argv[0]

def source_path():
    return sys.argv[1]

def template_path():
    return sys.argv[2]

def target_path():
    return sys.argv[-1]

def source_root():
    return os.environ['0CMS_SOURCE_ROOT']

# Exec a script into the the class namespace
class Importer():
    def __init__(self, path):
        # Import zcms into the namespace
        exec("from zcms.zcms import Markdown, Asciidoc, "
            "HtmlDiv, HtmlImage, Html, "
            "DirListing, Importer, "
            "source_path, template_path, target_path, source_root",
            self.__dict__, self.__dict__)
        # The following don't work at module level because they persist
        # across all files that import it.
        # Dependencies function
        self._dependencies = ()
        def Dependencies(*args):
            self._dependencies = args
        self.Dependencies = Dependencies
        # Replacements and GetReplacement functions
        self._replacements = {}
        self.Replacements = self._replacements.update
        # WebPage function
        self._webpage = ()
        def WebPage(*args):
            self._webpage = args
        self.WebPage = WebPage
        # Data function
        self._data = ""
        def Data(data):
            self._data = data
        self.Data = Data
        # Template function
        self._template = ""
        def Template(template):
            self._template = template
        self.Template = Template
        def GetTemplate():
            return self._template
        self.GetTemplate = GetTemplate
        # Import the file
        with open(path) as fp:
            try:
                exec(fp.read(), self.__dict__, self.__dict__)
            except Exception as e:
                print("0build: There's a problem with the file {}.  Details follow.".format(path))
                import traceback
                ex_type, ex, tb = sys.exc_info()
                print(traceback.print_exception(ex_type, ex, tb, file=sys.stdout))
                exit(1)
    # Methods called on the importer object (rather than within it, inside a page)
    # html function - get the final html
    def dependencies(self):
        return self._dependencies
    def replacements(self):
        return self._replacements.copy()
    def get_replacement(self, key):
        return self._replacements[key]
    def html(self):
        return '\n\n'.join([str(d()) if callable(d) else str(d)
            for d in self._webpage])
    def data(self):
        return str(self._data()) if callable(self._data) else str(self._data)
    def template(self):
        return str(self._template()) if callable(self._template) else str(self._template)

### Stuff for .0s pages to access when building a summary

class SiteItem():
    def __init__(self, path):
        self._source = path
        self._dir, self._file = os.path.split(path)

    def source(self):
        return self._source # return full path to source

    def url(self):
        # Get the directory relative to the site root
        reldir = relative_path(self._dir, source_root())
        base, ext = os.path.splitext(self._file)
        if ext in ('.0p', '.0s', '.0e'):
            target = os.path.join(reldir, base)
        else:
            target = os.path.join(reldir, self._file)
        return "/{target}".format(target=target)

class DirListing():
    def __init__(self, recursive=True, pattern='*'):
        # Find the directory of the page we're building
        self._doc_dir = os.environ['0CMS_CWD']

        if recursive:
            self.files = ()
            for directory, subdirs, files in os.walk(self._doc_dir):
                for f in fnmatch.filter(files, pattern):
                    self.files += (SiteItem(os.path.join(directory, f)),)
        else:
            entries = os.scandir(self._doc_dir)
            self.files = (SiteItem(os.path.join(self._doc_dir, e.name))
                for e in fnmatch.filter(entries, pattern)
                if e.is_file())
